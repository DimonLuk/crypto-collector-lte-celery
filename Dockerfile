FROM python:3.7.4-alpine3.10

ARG MODE
ENV MODE $MODE

ENV AWS_DEFAULT_REGION eu-west-1

RUN apk add musl-dev && apk add gcc && apk add curl-dev
RUN pip install pipenv

ADD ./Pipfile /code/Pipfile
ADD ./Pipfile.lock /code/Pipfile.lock
WORKDIR /code

RUN if [ "$MODE" = "development" ] ; then pipenv install --ignore-pipfile --system --dev ; fi
RUN if [ "$MODE" = "testing" ] ; then pipenv install --ignore-pipfile --system --dev ; fi
RUN if [ "$MODE" = "production" ] ; then pipenv install --ignore-pipfile --system ; fi
RUN if [ "$MODE" = "beat" ] ; then pipenv install --ignore-pipfile --system ; fi

ADD . /code
CMD ./docker-entrypoint.sh
