if [ "$MODE" = "development" ] ; then celery -A app.celery worker -B -l info ; fi
if [ "$MODE" = "testing" ] ; then pytest --junit-xml=test-report.xml --cov=app --cov-report=html --cov-report=term tests ; fi
if [ "$MODE" = "production" ] ; then celery -A app.celery worker -l info ; fi
if [ "$MODE" = "beat" ] ; then celery -A app.celery beat ; fi
