clear:
	rm -rf **/__pycache__

reset:
	rm .image_status

init:
	@echo "" >> .image_status

redis_db:
	docker-compose up -d redis_db

test_redis_db:
	docker-compose -f docker-compose.test.yml up -d redis_db

build: clear init
ifneq ($(shell cat .image_status),DEVELOPMENT)
	docker-compose build
	@echo DEVELOPMENT > .image_status
endif

build_test: clear init
ifneq ($(shell cat .image_status),TEST)
	docker-compose -f docker-compose.test.yml build
	@echo TEST > .image_status
endif

test: build_test test_redis_db
	docker-compose -f docker-compose.test.yml up -d
	docker-compose logs -f crypto_collector_lte_celery

intest: build_test test_redis_db
	docker-compose -f docker-compose.test.yml run crypto_collector_lte_celery /bin/sh

up: build redis_db
	docker-compose up -d

down: clear
	docker-compose down

logs:
	docker-compose logs

list:
	docker ps

restart: down up
	@echo Restarting

in: down build redis_db
	docker-compose run -p 8000:80 crypto_collector_lte_celery /bin/sh
