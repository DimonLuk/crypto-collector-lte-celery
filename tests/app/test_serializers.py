import pytest
from app.serializers import return_ticker_poloniex_serializer


@pytest.mark.freeze_time("2019-09-01")
def test_return_ticker_poloniex_serializer(
    return_ticker_poloniex_serializer_input_data,
    return_ticker_poloniex_serializer_output_data,
):
    result = return_ticker_poloniex_serializer(
        return_ticker_poloniex_serializer_input_data
    )
    assert result == return_ticker_poloniex_serializer_output_data
