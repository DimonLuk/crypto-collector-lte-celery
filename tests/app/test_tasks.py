from app.settings import settings
from app.tasks import poll_poloniex_and_send_to_collector


def test_poll_poloniex_and_send_to_collector(
    mock_requests, mock_return_ticker_poloniex_serializer
):
    with mock_requests("get", {"key": "value"}, False) as mocked_poloniex:
        with mock_requests(
            "post", {"another_key": "another_value"}, False
        ) as mocked_collector:
            poll_poloniex_and_send_to_collector()

            mocked_poloniex.assert_called_once_with(settings.POLONIEX_RETURN_TICKER_URL)

            mock_return_ticker_poloniex_serializer.assert_called_once_with(
                {"key": "value"}
            )

            mocked_collector.assert_called_once_with(
                settings.COLLECTOR_TICKER_URL, json={"data": "test"}
            )
