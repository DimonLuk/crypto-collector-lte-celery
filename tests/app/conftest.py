import pytest
from unittest.mock import Mock


@pytest.fixture
def mock_return_ticker_poloniex_serializer(monkeypatch):
    mock = Mock(return_value={"data": "test"})
    monkeypatch.setattr("app.tasks.return_ticker_poloniex_serializer", mock)
    return mock


@pytest.fixture
def return_ticker_poloniex_serializer_input_data():
    return {
        "BTC_BCN": {
            "id": 7,
            "last": "0.00000024",
            "lowestAsk": "0.00000025",
            "highestBid": "0.00000024",
            "percentChange": "0.04347826",
            "baseVolume": "58.19056621",
            "quoteVolume": "245399098.35236773",
            "isFrozen": "0",
            "high24hr": "0.00000025",
            "low24hr": "0.00000022",
        },
        "USDC_BTC": {
            "id": 224,
            "last": "6437.65329245",
            "lowestAsk": "6436.73575054",
            "highestBid": "6425.68259132",
            "percentChange": "0.00744080",
            "baseVolume": "1193053.18913982",
            "quoteVolume": "185.43611063",
            "isFrozen": "0",
            "high24hr": "6499.09114231",
            "low24hr": "6370.00000000",
        },
    }


@pytest.fixture
def return_ticker_poloniex_serializer_output_data():
    return {
        "tickers": [
            {
                "sell_currency": "BTC",
                "buy_currency": "BCN",
                "last": 2.4e-07,
                "lowest_ask": 2.5e-07,
                "highest_bid": 2.4e-07,
                "is_frozen": False,
                "base_volume": 58.19056621,
                "quote_volume": 245399098.35236773,
                "timestamp": "2019-09-01T00:00:00Z",
            },
            {
                "sell_currency": "USDC",
                "buy_currency": "BTC",
                "last": 6437.65329245,
                "lowest_ask": 6436.73575054,
                "highest_bid": 6425.68259132,
                "is_frozen": False,
                "base_volume": 1193053.18913982,
                "quote_volume": 185.43611063,
                "timestamp": "2019-09-01T00:00:00Z",
            },
        ]
    }
