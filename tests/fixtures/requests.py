import pytest
from unittest.mock import Mock
from contextlib import contextmanager


class MockRequestsResponse:
    def __init__(self, data, is_raise_for_status):
        self.data = data
        self.is_raise_for_status = is_raise_for_status

    def json(self):
        return self.data

    def raise_for_status(self):
        if self.is_raise_for_status:
            raise Exception
        else:
            pass


def _mock_requests(monkeypatch):
    @contextmanager
    def mock_requests_method(method, data, is_raise_for_status):
        import requests

        original_method = getattr(requests, method)
        mock = Mock(return_value=MockRequestsResponse(data, is_raise_for_status))
        monkeypatch.setattr(f"requests.{method}", mock)
        yield mock
        monkeypatch.setattr(f"requests.{method}", original_method)

    return mock_requests_method


@pytest.fixture
def mock_requests(monkeypatch):
    """
    Return a context manager that yields mock

    Parameters
    ----------
    method: str
        name of the method to be mocked ('get', 'post' and etc)

    data: dict
        dictionary which will be returned by json() method of the response

    is_raise_for_status: bool
        defines if `raise_for_status` will return an error

    Returns
    -------

    contextmanger which yields mock object
    """
    return _mock_requests(monkeypatch)
