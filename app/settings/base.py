from celery.schedules import crontab
import os


class BaseSettings:
    MODE = os.environ.get("MODE", "development")
    BROKER = os.environ.get("CELERY_BROKER", "redis://redis_db:6379/0")
    POLONIEX_RETURN_TICKER_URL = "https://poloniex.com/public?command=returnTicker"
    COLLECTOR_TICKER_URL = "https://crypto-collector-lte.ml/api/tickers/"
    BEAT_SCHEDULE = {
        "poll_poleniex_and_send_to_collector_every_15_mins": {
            "task": "app.tasks.poll_poloniex_and_send_to_collector",
            "schedule": crontab(minute="*/15"),
            "args": (),
        }
    }


settings_instance = BaseSettings()
