from .base import BaseSettings


class DevelopmentSettings(BaseSettings):
    BEAT_SCHEDULE = {
        "poll_poleniex_and_send_to_collector_every_15_mins": {
            "task": "app.tasks.poll_poloniex_and_send_to_collector",
            "schedule": 30,
            "args": (),
        }
    }


settings_instance = DevelopmentSettings()
