import os
from importlib import import_module


mode = os.environ.get("MODE")
if mode == "beat":
    module = import_module("app.settings.production")
else:
    module = import_module(f"app.settings.{mode}")
settings = module.settings_instance
