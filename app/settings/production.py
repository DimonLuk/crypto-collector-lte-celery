import os
from .base import BaseSettings
import logging
import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.celery import CeleryIntegration


sentry_logging = LoggingIntegration(level=logging.WARNING, event_level=logging.ERROR)
sentry_sdk.init(
    dsn="https://e5e6cd087109403ab8e6227c19c70ac8@sentry.io/1548120",
    integrations=[CeleryIntegration(), sentry_logging],
    debug=True,
)


class ProductionSettings(BaseSettings):
    MODE = "production"
    SUBMODE = os.environ.get("MODE")
    BROKER = "sqs://"


settings_instance = ProductionSettings()
