from .base import BaseSettings


class TestingSettings(BaseSettings):
    POLONIEX_RETURN_TICKER_URL = "https://some_testing_url.com"
    COLLECTOR_TICKER_URL = "https://another_testing_url.com"
    BEAT_SCHEDULE = {}


settings_instance = TestingSettings()
