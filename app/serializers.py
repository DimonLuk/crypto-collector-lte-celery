import datetime


def return_ticker_poloniex_serializer(raw_data):
    """
    Original schema:
        {
            "BTC_BCN": {
                "id": 7,
                "last": "0.00000024",
                "lowestAsk": "0.00000025",
                "highestBid": "0.00000024",
                "percentChange": "0.04347826",
                "baseVolume": "58.19056621",
                "quoteVolume": "245399098.35236773",
                "isFrozen": "0",
                "high24hr": "0.00000025",
                "low24hr": "0.00000022",
            },
            "USDC_BTC": {
                "id": 224,
                "last": "6437.65329245",
                "lowestAsk": "6436.73575054",
                "highestBid": "6425.68259132",
                "percentChange": "0.00744080",
                "baseVolume": "1193053.18913982",
                "quoteVolume": "185.43611063",
                "isFrozen": "0",
                "high24hr": "6499.09114231",
                "low24hr": "6370.00000000",
            },
        }
    Output schema (numbers are changed from strings to numbers, value stays the same):
        {
            "tickers": [
                    {
                        "sell_currency": "BTC",
                        "buy_currency": "BCN"
                        "last": 0.00000024,
                        "lowest_ask": "0.0000000025",
                        "highest_bid": 0.0000000024,
                        "is_frozen": True|False,
                        "base_volume": 58.123123,
                        "quote_volume": 214234234.123123123,
                        "timestamp": "2019-02-14T02:02:02"
                    }
                ],
        }
    """
    mappings = {
        "last": {"key": "last", "cast_to": float},
        "lowestAsk": {"key": "lowest_ask", "cast_to": float},
        "highestBid": {"key": "highest_bid", "cast_to": float},
        "isFrozen": {"key": "is_frozen", "cast_to": lambda x: x != "0"},
        "baseVolume": {"key": "base_volume", "cast_to": float},
        "quoteVolume": {"key": "quote_volume", "cast_to": float},
    }

    result = []

    for key, value in raw_data.items():
        obj = {}
        sell_currency, buy_currency = key.split("_")
        obj.update({"sell_currency": sell_currency, "buy_currency": buy_currency})
        for origin, mapped in mappings.items():
            obj[mapped["key"]] = mapped["cast_to"](value[origin])
        obj["timestamp"] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:00Z")
        result.append(obj)

    return {"tickers": result}
