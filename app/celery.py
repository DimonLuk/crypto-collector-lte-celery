from celery import Celery
from .settings import settings


app = Celery("app", broker=settings.BROKER, include=["app.tasks"])
app.conf.beat_schedule = settings.BEAT_SCHEDULE
