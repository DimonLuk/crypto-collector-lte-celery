import requests
from .settings import settings
from .celery import app
from .serializers import return_ticker_poloniex_serializer


@app.task
def poll_poloniex_and_send_to_collector():
    poloniex_response = requests.get(settings.POLONIEX_RETURN_TICKER_URL)
    poloniex_response.raise_for_status()
    raw_data = poloniex_response.json()
    data = return_ticker_poloniex_serializer(raw_data)
    collector_response = requests.post(settings.COLLECTOR_TICKER_URL, json=data)
    collector_response.raise_for_status()
